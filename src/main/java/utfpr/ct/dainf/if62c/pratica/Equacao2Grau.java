package utfpr.ct.dainf.if62c.pratica;

import java.math.BigDecimal;
/**
 * @author Marcelo Guimarães da Costa <marcelo@unicode.com.br> 
 */
public class Equacao2Grau <T extends Number> {
    private T a;
    private T b;
    private T c;
    
    public Equacao2Grau(T a, T b, T c) throws RuntimeException{
        if(((Number)a).doubleValue() == 0){
            throw new RuntimeException("Coeficiente a nao pode ser 0");
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public T getA() {
        return a;
    }
    public void setA(T a) throws RuntimeException{
        if(((Number)a).doubleValue() == 0){
            throw new RuntimeException("Coeficiente a nao pode ser 0");
        }
        this.a = a;
    }
    public BigDecimal getA_BigDecimal(){
        return new BigDecimal(a.doubleValue());
    }
    
    public T getB() {
        return b;
    }
    public void setB(T b) {
        this.b = b;
    }
    public BigDecimal getB_BigDecimal(){
        return new BigDecimal(b.doubleValue());
    }
    
    public T getC() {
        return c;
    }
    public void setC(T c) {
        this.c = c;
    }
    public BigDecimal getC_BigDecimal(){
        return new BigDecimal(c.doubleValue());
    }
    
    public double getRaiz1() throws RuntimeException{
        //if(b^2-4ac < 0)
        if(BigDecimal.valueOf(
                Math.pow(getB_BigDecimal().doubleValue(), 2)
            ).subtract(
                    BigDecimal.valueOf(4).multiply(getA_BigDecimal()).multiply(getC_BigDecimal())
            ).compareTo(BigDecimal.valueOf(0)) < 0){
            throw new RuntimeException("Equação não tem solução real");
        }
        //return (- getB_BigDecimal() + Math.sqrt(Math.pow(getB_BigDecimal(), 2) - 4 * getA_BigDecimal() * getC_BigDecimal())) / (2 * getA_BigDecimal());
        //(-b + V(b^2-4ac))/(2a)
        return getB_BigDecimal().multiply(BigDecimal.valueOf(-1))
                .add(
                    BigDecimal.valueOf(
                        Math.sqrt(
                            (BigDecimal.valueOf(
                                Math.pow(getB_BigDecimal().doubleValue(), 2)
                            ).subtract(
                                    BigDecimal.valueOf(4).multiply(getA_BigDecimal()).multiply(getC_BigDecimal())
                            )).doubleValue()
                        )
                    ) 
                ).divide(
                    getA_BigDecimal().multiply(BigDecimal.valueOf(2))
                ).doubleValue();
    }
    public double getRaiz2() throws RuntimeException{
        //if(b^2-4ac < 0)
        if(BigDecimal.valueOf(
                Math.pow(getB_BigDecimal().doubleValue(), 2)
            ).subtract(
                    BigDecimal.valueOf(4).multiply(getA_BigDecimal()).multiply(getC_BigDecimal())
            ).compareTo(BigDecimal.valueOf(0)) < 0){
            throw new RuntimeException("Equação não tem solução real");
        }
        //return (- getB_BigDecimal() + Math.sqrt(Math.pow(getB_BigDecimal(), 2) - 4 * getA_BigDecimal() * getC_BigDecimal())) / (2 * getA_BigDecimal());
        //(-b - V(b^2-4ac))/(2a)
        return getB_BigDecimal().multiply(BigDecimal.valueOf(-1))
                .subtract(
                    BigDecimal.valueOf(
                        Math.sqrt(
                            (BigDecimal.valueOf(
                                Math.pow(getB_BigDecimal().doubleValue(), 2)
                            ).subtract(
                                    BigDecimal.valueOf(4).multiply(getA_BigDecimal()).multiply(getC_BigDecimal())
                            )).doubleValue()
                        )
                    ) 
                ).divide(
                    getA_BigDecimal().multiply(BigDecimal.valueOf(2))
                ).doubleValue();
    }
}
